let scores = [100, 100, 85, 100, 60, 85, 100, 90, 55, 75, 100];

const countOccurances = function(arr, val)
{
    return arr.reduce((acc, elem) => {
        return (val === elem ? acc + 1 : acc)
    }, 0);
};
console.log(countOccurances(scores, 100));
console.log(countOccurances(scores, 85));
