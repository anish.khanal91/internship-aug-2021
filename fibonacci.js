const fibonacci = function (n) 
{
  if (n===1) 
  {
    return [0, 1];
  } 
  else 
  {
    const num = fibonacci(n - 1);
    num.push(num[num.length - 1] + num[num.length - 2]);
    return num;
  }
};

 console.log(fibonacci(5));