document.querySelector('#form-select').addEventListener('submit', handleSubmitForm);
document.addEventListener('click',function(e){
//    console.log(e.target);
    if(e.target && e.target.id== 'deleteitem'){
        // console.log('delete');
        deleteTodo(e);
     }
 });
// document.querySelector('ul').addEventListener('click', handleClickDeleteOrCheck);
// document.querySelector('#delete-item').addEventListener('click',deleteitem);
// document.getElementById('clear').addEventListener('click',clear);
// document.getElementById('Submit').addEventListener('click', (event) => {
//     event.preventDefault();
//     console.log(document.getElementById('start').value)
// })

function handleSubmitForm(e) {
    e.preventDefault();
    let input = document.querySelector('#task-input');
    let date = document.querySelector('#start');
    if (input.value != '')
        addTodo(input.value, date.value);
    input.value = ''
}
function deleteitem(e) {
    // if (e.target.name == 'checkButton')
    //     checkTodo(e);
    // if (e.target.name == 'deleteButton')
    // console.log('deleteitem')
    deleteTodo(e);
}

function addTodo(todo, date) {
    let ul = document.querySelector('ul');
    let li = document.createElement('li');

    li.innerHTML = `
        <span class="todo-item">${todo} ${date}</span>
        <button name="checkButton" id="check-item"><i class="fas fa-check-square"></i></button>
        <button name="deleteButton"><i  id="deleteitem" class="fas fa-trash"></i></buttom>
`;
    li.classList.add('todo-list-item');
    ul.appendChild(li);
}
function checkTodo(e) {
    let item = e.target.parentNode;
    if (item.style.textDecoration == 'line-through')
        item.style.textDecoration = 'none';
    else
        item.style.textDecoration = 'line-through';
}

function deleteTodo(e) {
    let item = e.target.parentElement;
    
    console.log(item);
    item.addEventListener('transitionend', function () {
        item.remove();
    })
    console.log(e.target.parentNode.parentElement);
item.classList.add('todo-list-item-fall');
}
