const express = require('express')
const Blog = require('./model/blog')
const path = require('path')
const app = express()
const port = 8000
const mongoose = require('mongoose');
const { title } = require('process')
const mongo = 'mongodb+srv://anishkhanal91:anishkhanal91@noe3tuts.hwax4.mongodb.net/test'
mongoose.connect(mongo, {useNewUrlParser: true, useUnifiedTopology: true })
  .then((result) => app.listenerCount(8000))
  .catch((err) => console.log(err));

app.get('/add-blog', (req,res) => {
  const blog = new Blog({
    title: 'new blog',
      snippet: 'about my new blog',
    body: 'more about my new blog',
  });
 
  Blog.save()
  .then((result) => {
    res.send(result)
  })
  .catch((err) => {
    console.log(err);
  });
})


app.set('view engine','ejs');

app.get('/',(req,res) =>{
  res.sendFile(path.join(__dirname,"index.ejs"))
})

app.get('/', (req, res) => {
  const blogs = [
    {title: 'Yoshi finds eggs', snippet: 'lorem ipsum dolor sit amet consectetur'},
    {title: 'Yoshi finds eggs', snippet: 'lorem ipsum dolor sit amet consectetur'},
    {title: 'Yoshi finds eggs', snippet: 'lorem ipsum dolor sit amet consectetur'},
  ];
  res.sendFile(path.join(__dirname,"index.ejs"))
})
app.get('/hello/:name',(req,res) =>{
  res.send("Hello " + req.params.name)
})

app.get('/profile/:name',(req,res) =>{
  res.send("Profile " + req.params.name)
})

app.get('/about',(req,res) =>{
  res.sendFile(path.join(__dirname,"about.ejs"))
})

app.get('/error',(req,res) =>{
  res.sendFile(path.join(__dirname,"error.ejs"))
})

app.listen(port, ()=>{
  console.log(`Node js running at http://localhost:${port}/`)
}) 